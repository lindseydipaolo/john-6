import '../../src/Home.css';

interface HamburgerProps {
  onClick: () => void;
}

function Hamburger({ onClick }: HamburgerProps) {
  return (
    <div onClick={onClick}>
      <div className="hamburgerBar"></div>
      <div className="hamburgerBar"></div>
      <div className="hamburgerBar"></div>
    </div>
  );
}

export default Hamburger;
