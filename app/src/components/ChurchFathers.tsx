import '../../src/Home.css';

function ChurchFathers() {
  return (
    <div>
      <h1>Church Fathers</h1>
      <p className="churchFathersInfo">
        <p>
          "Do not, therefore, regard the Bread and the Wine as simply that; for they are, according to the Master's 
          declaration, the Body and Blood of Christ. Even though the senses suggest to you the other, let faith make 
          you firm. Do not judge in this matter by taste, but -- be fully assured by the faith, not doubting that 
          you have been deemed worthy of the Body and Blood of Christ."
          <br></br>-St Cyril of Jerusalem (Catechetical Lectures, c. 313 - 386)
        </p>
        <p>
          "That Bread which you see on the altar, having been sanctified by the word of God is the body of Christ. 
          That chalice, or rather, what is in that chalice, having been sanctified by the word of God, is the blood
          of Christ. Through that bread and wine the Lord Christ willed to commend his body and blood, which he poured 
          out for us unto the forgiveness of sins."
          <br></br>-St Augustine of Hippo (Sermons 227)
        </p>
        <p>
          "What you see is the bread and the chalice; that is what your own eyes report to you. But what your faith 
          obliges you to accept is that the bread is the body of Christ and the chalice [wine] the blood of Christ."
          <br></br>-St Augustine of Hippo (Sermons 272)
        </p>
        <p>
          "You may perhaps say: 'My bread is ordinary.' But that bread is bread before the words of the Sacraments; 
          where the consecration has entered in, the bread becomes the flesh of Christ. And let us add this: How can 
          what is bread be the Body of Christ? By the consecration. The consecration takes place by certain words; but 
          whose words? Those of the Lord Jesus. Like all the rest of the things said beforehand, they are said by the 
          priest; praises are referred to God, prayer of petition is offered for the people, for kings, for other persons; 
          but when the time comes for the confection of the venerable Sacrament, then the priest uses not his own words 
          but the words of Christ. Therefore it is the word of Christ that confects this Sacrament."
          <br></br>-St. Ambrose of Milan (c. 333 - 397)
        </p>
      </p>
    </div>
  );
}

export default ChurchFathers;
