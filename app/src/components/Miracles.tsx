import '../../src/Home.css';

function Miracles() {
  return (
    <div>
        <h1>Miracles</h1>
        <p className="miracleInfo">
          Bread and wine transform into Christ's body and blood at consecration but still look and taste
          like bread and wine. Normally human senses do not perceive any change. However, there are over 
          100 miraculous instances where the Lord allowed the transformation to appear more obviously. 
          The Eucharistic miracles in Lanciano, Buenos Aires, and Sokolka provide scientific evidence in 
          favor of the Real Presence.
        </p>
        <div className="menuItems">
          <a href="/miracles/italy">
            <div className="menuItem">
              Lanciano, Italy - 750 AD
            </div>
          </a>
          <a href="/miracles/argentina">
            <div className="menuItem">
              Buenos Aires, Argentina - 1996
            </div>
          </a>
          {/* <a href="/miracles/mexico">
            <div className="menuItem">
              Tixtla, Mexico - 2006
            </div>
          </a> */}
          <a href="/miracles/poland">
            <div className="menuItem">
              Sokolka, Poland - 2008
            </div>
          </a>
        </div>
    </div>
  );
}

export default Miracles;
