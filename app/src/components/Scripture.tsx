import '../../src/Home.css';

function Scripture() {
  return (
    <div>
        <h1>Scripture</h1>
        <div className="scriptureInfo">
          <p>
            "I am the bread of life; he who comes to me shall not hunger, and he who believes
            in me shall never thirst." <br></br>John 6:35
          </p>

          <p>
            "I am the living bread which came down from heaven; if anyone eats of this bread,
            he will live forever; and the bread which I shall give for the life of the world
            is my flesh." <br></br>John 6:51
          </p>

          <p>
            "Truly, truly I say to you, unless you eat the flesh of the Son of man and drink
            his blood, you have no life in you; he who eats my flesh and drinks my blood has
            eternal life, and I will raise him up at the last day. For my flesh is food indeed,
            and my blood is drink indeed. He who eats my flesh and drinks my blood abides in me,
            and I in him." <br></br>John 6:53-56
          </p>

          <p className="scriptureReflection">
            John 6:22-59 is known as "The Bread of Life Discourse." Jesus tells the crowd he wants to 
            give them his body and blood to eat and drink. Many of his disciples stopped following 
            him because the idea of drinking his blood scandalized them. Because he meant what he said, 
            and because he respected their free will, he allowed them to walk away. When Jesus taught
            parables to crowds, he explained their symbolism to his apostles privately. He did not
            do so after the Bread of Life Discourse because the Eucharist is truly Christ himself.
          </p>
        </div>
    </div>
  );
}

export default Scripture;
