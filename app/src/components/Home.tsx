import '../../src/Home.css';

function Home() {
  return (
    <div>
        <h1>The Eucharist</h1>
        <p className="introParagraph">
          "Take this, all of you, and eat of it. For this is my body, which will be 
          given up for you." These are the words of consecration that Christ speaks
          through priests at mass, and they bring about a powerful change. Bread and wine 
          truly become the Body and Blood of Christ. The Real Presence of Jesus in the 
          Eucharist is highlighted in Scripture, in miraculous accounts, and in teachings 
          of the Early Church Fathers.
        </p>
        <div className="menuItems">
          <a href="/scripture">
            <div className="menuItem">
              Scripture
            </div>
          </a>
          <a href="/miracles">
            <div className="menuItem">
              Miracles
            </div>
          </a>
          <a href="/churchfathers">
            <div className="menuItem">
              Church Fathers
            </div>
          </a>
        </div>
    </div>
  );
}

export default Home;
