import '../../src/Home.css';

function Poland() {
  return (
    <div>
      <div className="backButton">
        <a href="/miracles">
          &#x25c0; Miracles
        </a>
      </div>
      <h1>Sokolka, Poland</h1>
      <div className="miracleInfo">
      <p>
        On October 12, 2008, a consecrated host fell on the ground during mass at St Anthony Catholic Church
        in Sokolka, Poland. A religious sister who served at the church, following protocol, placed the host 
        in a small vessel of water. Then she placed the vessel in a safe to allow the host to dissolve. One week 
        later she opened to the safe to find that, although most of the host had dissolved, a small piece remained.
        Connected with it was a mysterious red substance.
      </p>
      <p>
        The sister brought it to the pastor's attention. With orders from the Archbishop, the pastor carefully
        removed the host with the red substance from the water with a spoon and placed them on a pure white
        cloth. When the cloth dried, the administrative office of the Archdiocese consulted two professors and 
        pathological anatomy specialists at the Medical University of Bialystok to investigate.
      </p>
      <p>
        The scientists, Professor Maria Elzbieta Sobaniec-Lotowska and Professor Stanislaw Sulkowski, each studied 
        a fragment of the mysterious red substance independently of one another. An&nbsp;
        <a 
          href="https://www.researchgate.net/publication/330400580_Eucharistic_miracle_from_the_scientific_perspective"
          target="blank"
          className="aaa"
        >
          article
        </a>
        &nbsp;published on ResearchGate explains how their findings "are consistent and indicate the presence of human 
        cardiac muscle tissue with specific pathomorphological features."
      </p>
      <p>
        So the mysterious red substance was human heart tissue. Astonishingly, the tissue was still alive despite
        the time that had passed between the mass on October 12 2008 and the scientists' investigations in January
        2009. Theologically, this signifies Christ as the Living Bread and the Eucharist as the heart of the Church.
      </p>
      </div>
    </div>
  );
}

export default Poland;
