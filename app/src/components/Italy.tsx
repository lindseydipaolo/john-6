import '../../src/Home.css';

function Italy() {
  return (
    <div>
        <div className="backButton">
            <a href="/miracles">
            &#x25c0; Miracles
            </a>
        </div>
        <h1>Lanciano, Italy</h1>
        <p className="miracleInfo">
          Coming soon!
        </p>
    </div>
  );
}

export default Italy;
