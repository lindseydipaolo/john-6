import { useEffect, useRef, useState } from 'react';
import '../../src/Home.css';
import Hamburger from './Hamburger';

function Navbar() {
  const [showNavbarOptions, setShowNavbarOptions] = useState(false);
  const menuRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (showNavbarOptions) {
      const handleClickOutside = (event: { target: any; }) => {
        if (menuRef.current && !menuRef.current.contains(event.target)) {
          setShowNavbarOptions(false);
        }
      };

      document.addEventListener('mousedown', handleClickOutside);

      return () => {
        document.removeEventListener('mousedown', handleClickOutside);
      };
    }
  }, [showNavbarOptions]);
  
  return (
    <>
      <div className="navbar">
        <Hamburger onClick={() => setShowNavbarOptions(!showNavbarOptions)}/>
      </div>
      { showNavbarOptions &&
        <div className="navbarOptions" ref={menuRef}>
          <a href="/">
            <div className="navbarOption">
              Home &#x25B6;
            </div>
          </a>
          <a href="/scripture">
            <div className="navbarOption">
              Scripture &#x25B6;
            </div>
          </a>
          <a href="/miracles">
            <div className="navbarOption">
              Miracles &#x25B6;
            </div>
          </a>
          <a href="/churchfathers">
            <div className="navbarOption">
              Church Fathers &#x25B6;
            </div>
          </a>
        </div>
      }

    </>
  );
}

export default Navbar;
