import './App.css';
import Home from './components/Home';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Scripture from './components/Scripture';
import Miracles from './components/Miracles';
import ChurchFathers from './components/ChurchFathers';
import Navbar from './components/Navbar';
import Italy from './components/Italy';
import Argentina from './components/Argentina';
// import Mexico from './components/Mexico';
import Poland from './components/Poland';

function App() {
  return (
    <div className="App">
      <Navbar />
      <header className="App-menu">
        <div>
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/scripture" element={<Scripture />} />
              <Route path="/miracles" element={<Miracles />} />
              <Route path="/churchfathers" element={<ChurchFathers />} />
              <Route path="/miracles/italy" element={<Italy />} />
              <Route path="/miracles/argentina" element={<Argentina />} />
              {/* <Route path="/miracles/mexico" element={<Mexico />} /> */}
              <Route path="/miracles/poland" element={<Poland />} />
            </Routes>
          </BrowserRouter>
        </div>
      </header>
    </div>
  );
}

export default App;
